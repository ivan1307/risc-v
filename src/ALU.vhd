library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu is

generic (N : integer :=64);
port (
      A_i      : in  std_logic_vector(N - 1 downto 0);
      B_i      : in  std_logic_vector(N - 1 downto 0);
      OUTPUT_o : out std_logic_vector(N - 1 downto 0);
      ALUop_i  : in  std_logic_vector(3 downto 0);
      Zero_o   : out std_logic
      );
end entity;

architecture alu_arch of alu is

constant ADD_IN : std_logic_vector(3 downto 0) := "0000";  -- As defined in the book
constant SUB_IN : std_logic_vector(3 downto 0) := "0001";  -- As defined in the book
constant SLL_IN : std_logic_vector(3 downto 0) := "0010";
constant XOR_IN : std_logic_vector(3 downto 0) := "0011";
constant SRL_IN : std_logic_vector(3 downto 0) := "0100";
constant SRA_IN : std_logic_vector(3 downto 0) := "0101";
constant OR_IN  : std_logic_vector(3 downto 0) := "0110";  -- As defined in the book
constant AND_IN : std_logic_vector(3 downto 0) := "0111";  -- As defined in the book
constant DIFERENTE_IN  : std_logic_vector(3 downto 0) := "1000";
constant MENOR_IN      : std_logic_vector(3 downto 0) := "1001";
constant MAYORIGUAL_IN : std_logic_vector(3 downto 0) := "1010";
constant MENORsinsigno_IN      : std_logic_vector(3 downto 0) := "1011";
constant MAYORIGUALsinsigno_IN : std_logic_vector(3 downto 0) := "1100";

--constant m: std_logic_vector(N-1 downto 0) := (others => '0'); -- cero de 64 bits


signal aux : std_logic_vector(N-1 downto 0);
signal output_s : std_logic_vector(N-1 downto 0);
signal cero : std_logic;

begin

OUTPUT_o <= output_s;
Zero_o <= cero ;

process(A_i, B_i, ALUop_i, output_s, aux, cero)
constant m: std_logic_vector(N-1 downto 0) := (others => '0'); -- cero de 64 bits

begin
    case ALUop_i is
        when ADD_IN =>
            output_s <= std_logic_vector((unsigned(A_i) + unsigned(B_i))); 
            cero <= '0';
	when SUB_IN =>
            output_s <= std_logic_vector((signed(A_i) - signed(B_i)));
	    if (output_s = m) then
		   cero <= '1';
	    else 
		   cero <= '0';
	    end if;
	when SLL_IN =>
            output_s <= std_logic_vector(shift_left(unsigned(B_i), to_integer(unsigned(A_i))));
            cero <= '0';
	when XOR_IN =>
            output_s <= A_i xor B_i;
            cero <= '0';
	when SRL_IN =>
            output_s <= std_logic_vector(shift_right(unsigned(B_i), to_integer(unsigned(A_i))));
            cero <= '0';
	when SRA_IN =>
            output_s <= std_logic_vector(shift_right(signed(B_i), to_integer(unsigned(A_i))));
            cero <= '0';
	when OR_IN =>
            output_s <= A_i or B_i;
            cero <= '0';
        when AND_IN =>
            output_s <= A_i and B_i;
            cero <= '0';
	when DIFERENTE_IN =>
	    output_s <= (others => '0');
            aux<= std_logic_vector((unsigned(A_i) - unsigned(B_i)));
	    if ( aux /= m) then
		   cero <= '1';
	    else 
		   cero <= '0';
	    end if;
        when MAYORIGUAL_IN =>
            output_s <= (others => '0');
	    if (A_i >= B_i ) then
	        cero  <= '0';
	    else 
	        cero  <= '1';
	    end if;
	when MAYORIGUALsinsigno_IN =>
            output_s <= (others => '0');
	    if (unsigned(A_i) >= unsigned(B_i) ) then
	        cero  <= '1';
	    else 
	        cero  <= '0';
	    end if;
        when MENOR_IN =>
	    output_s <= (others => '0');
	    if (A_i < B_i) then
		cero <= '0';
	    else 
	   	cero <= '1';
	    end if;

	when MENORsinsigno_IN =>
	    output_s <= (others => '0');
	    if (unsigned(A_i) < unsigned(B_i)) then
		cero <= '1';
	    else 
	   	cero <= '0';
	    end if;
	when others => output_s <= (others => '0');        
    end case;
        
  end process;
end alu_arch;