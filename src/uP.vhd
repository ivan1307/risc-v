--------------------------------------------------------------------------------
-- Se escriben las librerias y paquetes
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use std.textio.all;

--------------------------------------------------------------------------------
entity risc_v is
    port(
        CLK_i           : in std_logic;
        RST_i           : in std_logic
        );
    end;

    -- Arquitectura del procesador
    architecture arch_processeur of risc_v is
--------------------------------------------------------------------------------
    -- Defino señales de control (ver pag 256)


--Señales de control
    signal Branch    :     std_logic;                     --UC -> AND 
    signal ZERO: std_logic;                               --ALU -> AND 
    signal ALUSrc   :     std_logic;                      --UC -> mux1
    signal MemtoReg  :     std_logic_vector(1 downto 0);  --UC -> mux2
    signal ALUOp    :     std_logic_vector (3 downto 0);  --UC -> ALU
    signal MemWrite,MemRead:    std_logic;                --UC -> memoria de datos
    signal RegWrite:           std_logic;                 --UC -> banco de registros
    signal Mux_PC: std_logic_vector(1 downto 0);          --UC -> mux2
    signal Bitlui: std_logic;                             --UC -> mux1
    
--Señales Internas
    signal SUMtoMUX: std_logic_vector (63 downto 0);      --Sum_pc_imm-->Muxprogramcounter
    signal MuxcondPC: std_logic_vector (63 downto 0);     --Muxand-->Muxprogramcounter
    signal MUXtoPC: std_logic_vector(63 downto 0);        --Muxprogramcounter-->PC;
    signal Instruction: std_logic_vector (31 downto 0);   --Instruccion Mem-->Banco de registro
    signal PCOUT: std_logic_vector (63 downto 0);         --PC-->Memoria de instruccion;
    signal ImmGenOUT: std_logic_vector(63 downto 0);      --Salida del Generador de inmediato
    signal MUXtoREGISTERS: std_logic_vector(63 downto 0); --Mux-->Memoria de registro;
    signal R_a,R_b: std_logic_vector(63 downto 0);        --Salida de los registros
    signal ALUresult: std_logic_vector(63 downto 0);      --Salida de la ALU
    signal MuxtoALU_a: std_logic_vector(63 downto 0);     --MUXa-->ALU
    signal MuxtoALU_b: std_logic_vector(63 downto 0);     --MUXb-->ALU
    signal DataMEMtoMux :  std_logic_vector (63 downto 0);--Memoria de datos-->MUXregister
    signal SHIFTED_LEFT: std_logic_vector(63 downto 0);   --Shifteo x2
    signal AND_OUT: std_logic;                            --Salida de la AND


component UC is
    generic(
        opcode  :   integer :=  32
        );
    port (
        INSTR_i         :   in  std_logic_vector(opcode-1 downto 0);            -- Instruccion de entrada
        Branch_o        :   out std_logic;                                      -- Alto condicional
        MemRead_o       :   out std_logic;                                      -- Lectura de memoria
        MemtoReg_o      :   out std_logic_vector(1 downto 0);                   -- Memoria a registro
        ALUop_o         :   out std_logic_vector(3 downto 0);                   -- Seleccion de operacion de ALU
        MemWrite_o      :   out std_logic;                                      -- Escritura de memoria
        ALUsrc_o        :   out std_logic;                                      -- Selecciona entre un inmediato y un registro
        Bitlui_o        :   out std_logic;                                      -- Permite utilizar lui
        Reg_W_o         :   out std_logic;                                      -- Escritura de registro  
        Mux_Pc_o        :   out std_logic_vector (1 downto 0)                   -- Elige la entrada al PC  
    );                                 
end component UC;

component registerbank is
    generic(size_of_register     : integer := 64;    -- cantidad de bits registros
            amount_registers_dir : integer := 5);    -- cantidad de bits de direccionamiento a registros
    port (
        A_i     : IN     std_logic_vector(amount_registers_dir - 1 downto 0);
        B_i     : IN     std_logic_vector(amount_registers_dir - 1 downto 0);
        C_i     : IN     std_logic_vector(amount_registers_dir - 1 downto 0);
        Reg_W_i : IN     std_logic;
        RST_i   : IN     std_logic;
        CLK_i   : IN     std_logic;  
        W_c_i   : IN     std_logic_vector(size_of_register - 1 downto 0);
        R_a_o   : OUT    std_logic_vector(size_of_register - 1 downto 0);
        R_b_o   : OUT    std_logic_vector(size_of_register - 1 downto 0)
        );
end component registerbank;


component immgen is
    generic(len_i : integer := 32;   -- Longitud de la instruccion de entrada
            len_o : integer := 64);  -- Longitud de la instruccion de salida
    port (
        Inst_i : IN     std_logic_vector(len_i - 1 downto 0);  -- Instruccion de entrada
        Inmed_o : OUT    std_logic_vector(len_o - 1 downto 0)  -- Inmediato de salida
    );
end component immgen;
 
      
component datamem is
    generic(ADDR_WIDTH : integer := 10;
        MEMO_SIZE  : integer := 1024;
        DATA_WIDTH : integer := 64);
    port (
        CLK_i      : in  std_logic;
        ADDR_i     : in  std_logic_vector (ADDR_WIDTH-1 downto 0);
        DATA_i     : in  std_logic_vector (DATA_WIDTH-1 downto 0);
        DATA_o     : out std_logic_vector (DATA_WIDTH-1 downto 0);
        MemWrite_i : in  std_logic;
        MemRead_i  : in  std_logic
    );
end component datamem;


component alu is
    generic(N : integer :=64);
    port (
        A_i      : in  std_logic_vector(N - 1 downto 0);
        B_i      : in  std_logic_vector(N - 1 downto 0);
        OUTPUT_o : out std_logic_vector(N - 1 downto 0);
        ALUop_i  : in  std_logic_vector(3 downto 0);
        Zero_o   : out std_logic
    );
end component alu;


component programcounter is
    generic (DATA_SIZE: integer :=64);
    port (
        DATA_i   : in  std_logic_vector(DATA_SIZE-1 downto 0);
        EN_i     : in  std_logic;
        RST_i    : in  std_logic;
        CLK_i    : in  std_logic;
        DATA_o   : out std_logic_vector(DATA_SIZE-1 downto 0)
    );
end component programcounter;


component progmem is
    generic(
        ancho_inst:     integer := 8;
        ancho_address:  integer := 6
    );
    port (
        CLK_i:  in  std_logic;
        RST_i:  in  std_logic;
        ADDR_i: in  std_logic_vector(ancho_address-1 downto 0);
        DATA_o: out std_logic_vector((4*ancho_inst)-1 downto 0)
    );
end component progmem;

    begin

        PC: programcounter generic map (DATA_SIZE => 64)

        port map (

                Data_i => MUXtoPC,
                EN_i => '1',
                RST_i =>RST_i,
                CLK_i =>CLK_i,
                Data_o => PCOUT

                );

        PM: ProgMem generic map (
                ancho_inst    => 8,
                ancho_address => 10
                )
                port map(
                RST_i => RST_i,
                CLK_i => CLK_i,
                ADDR_i => PCOUT (9 downto 0),
                Data_o => Instruction

            );

        ControlUnit: UC
        generic map(opcode => 32)
        port map(

            INSTR_i => Instruction(31 downto 0), --INSTR_i es un vector de 32 bit en lugar de un vector de 7 bit.
            Branch_o => Branch,
            MemRead_o => MemRead,
            MemtoReg_o => MemtoReg,
            ALUop_o =>  ALUOp, --ALUop_o es un vector de 4 bit en lugar de un vector 2 bit.
            MemWrite_o => MemWrite,
            ALUsrc_o => ALUSrc,
            Reg_W_o => RegWrite,
            Bitlui_o  => Bitlui,
            Mux_Pc_o => Mux_Pc
            );

        Registers: registerbank
        generic map(
            size_of_register  => 64,  -- cantidad de bits registros
            amount_registers_dir => 5    -- cantidad de bits de direccionamiento a registros
            )
        port map(

            A_i => Instruction(19 downto 15),
            B_i => Instruction(24 downto 20),
            C_i => Instruction(11 downto 7),
            Reg_W_i => RegWrite,
            RST_i =>  RST_i,
            CLK_i =>  CLK_i,
            W_c_i => MUXtoREGISTERS,
            R_a_o => R_a,
            R_b_o => R_b

            );

 	ImmediateGenerator: ImmGen
        generic map(
            len_i => 32,  -- Longitud de la instruccion de entrada
            len_o => 64
            )
        port map(
            Inst_i => Instruction(31 downto 0),
            Inmed_o => ImmGenOUT (63 downto 0)
            );
              
        ALU1: alu
        generic map(N => 64)

        port map (

            A_i => MuxtoALU_a,
            B_i => MuxtoALU_b,
            OUTPUT_o => ALUresult(63 downto 0),
            ALUop_i => ALUOp,
            Zero_o =>  ZERO

            );

        DataMemory: datamem
        generic map (
            ADDR_WIDTH => 64,
            MEMO_SIZE  => 1024,
            DATA_WIDTH => 64
                )
        port map(

                CLK_i => CLK_i,
                ADDR_i => ALUresult(63 downto 0),
                DATA_i => R_b,
                DATA_o => DataMEMtoMux,
                MemWrite_i => MemWrite,
                MemRead_i => MemRead
            );


SHIFTED_LEFT<=std_logic_vector(shift_left(signed(ImmGenOUT), 2));
AND_OUT <= Branch and ZERO;
SUMtoMUX <= std_logic_vector(signed(SHIFTED_LEFT)+signed(PCOUT));

MuxtoALU_a <= ImmGenOUT when ALUSrc = '1' else R_b;
MuxtoALU_b <= (others => '0') when Bitlui = '1' else R_a;
MuxcondPC <= std_logic_vector(unsigned(PCOUT) + 4) when AND_OUT = '0' else std_logic_vector(signed(SHIFTED_LEFT)+signed(PCOUT));

MUXtoREGISTERS <= std_logic_vector(unsigned(PCOUT) + 4) when MemtoReg = "00" else 
                  DataMEMtoMux when MemtoReg = "01" else
                  ALUresult when MemtoReg = "10" else 
                  "0000000000000000000000000000000000000000000000000000000000000000";
MUXtoPC <= SUMtoMUX when Mux_Pc= "00" else 
           std_logic_vector(signed(PCOUT) + 4) when Mux_Pc= "01" else
           MuxcondPC when Mux_Pc= "10" else 
           std_logic_vector(shift_left(signed(ALUresult), 2));
 
end architecture arch_processeur;



 