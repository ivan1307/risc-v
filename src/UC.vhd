library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity UC is
    generic(
        opcode  :   integer :=  32
    );
    port(
        INSTR_i         :   in  std_logic_vector(opcode-1 downto 0);            -- instruccion de entrada
        Branch_o        :   out std_logic;                                      -- salto condicional
        MemRead_o       :   out std_logic;                                      -- lectura de memoria
        MemtoReg_o      :   out std_logic_vector(1 downto 0);                   -- memoria a registro
        ALUop_o         :   out std_logic_vector(3 downto 0);                   -- seleccion de operacion de ALU
        MemWrite_o      :   out std_logic;                                      -- escritura de memoria
        ALUsrc_o        :   out std_logic;                                      -- selecciona entre un inmediato y un registro
        Bitlui_o        :   out std_logic;                                      -- permite utilizar lui
        Reg_W_o         :   out std_logic;                                       -- escritura de registro  
        Mux_Pc_o        :   out std_logic_vector(1 downto 0)                                  
    );
end UC;

architecture control_arch of UC is
    signal opcode_i:    std_logic_vector(6 downto 0);   --opcode de instruccion, hasta 7 bits
    signal func3:       std_logic_vector(2 downto 0);   -- func3 de instruccion, hasta 3 bits 
    signal func7:       std_logic_vector(6 downto 0);   -- func7 de instruccion, hasta 7 bits  
    begin
    opcode_i <= INSTR_i(6 downto 0);      -- se le otorga la parte que corresponde al opcode
    func3 <= INSTR_i(14 downto 12);       -- se le ortorga la parte que corresponde a func3
    func7 <= INSTR_i (31 downto 25);      -- se le otorga la parte qu corresponde a func7   
    
    Branch_o    <= '1' when ((opcode_i = "1100011" and func3= "000") or (opcode_i = "1100111" and (func3= "001" or func3= "100" or func3= "101" or func3= "110" or func3= "111"))) else '0';

   
    MemRead_o   <= '1' when (opcode_i = "0000011" or (opcode_i = "1100111" and func3= "000") or (opcode_i = "0010011" and(func3= "110" or func3="111"))) else '0';
    
    MemtoReg_o  <= "00" when (opcode_i = "1100111" and func3= "000") or (opcode_i = "1101111") else 
	           "01" when (opcode_i = "0000011") else 
                   "10" when (opcode_i = "0010011" or opcode_i = "0110011"or opcode_i="0110111") else "11";
 
    MemWrite_o  <= '1' when (opcode_i = "0100011") else '0';
    

    ALUsrc_o    <= '1' when (opcode_i="0000011" or opcode_i = "0010011" or (opcode_i= "1100111" and func3= "000") or opcode_i= "0100011" or opcode_i= "0110111" or opcode_i= "1101111") else '0'; -- verificar jalr y jal 
    

    Reg_W_o     <= '1' when (opcode_i="0110011" or opcode_i = "1101111" or opcode_i="0000011" or opcode_i= "0010011" or (opcode_i= "1100111" and func3= "000") or opcode_i= "0110111") else '0';
    
    Bitlui_o <= '1' when (opcode_i="0110111") else '0';

    Mux_Pc_o <= "00" when (opcode_i = "1101111") else 
	        "01" when (opcode_i="0110011" or opcode_i="0000011" or opcode_i="0010011" or opcode_i="0100011" or opcode_i="0110111") else 
                "10" when ((opcode_i = "1100111" and (func3= "001" or func3= "100" or func3= "101" or func3= "110" or func3= "111")) or opcode_i = "1100011") else "11";



--------------------------------------------------------------------------------
    -- ALUop_o 
    
    -- 0000 ADD 
    -- 0001 SUB 
    -- 0010 SLL
    -- 0011 XOR
    -- 0100 SRL
    -- 0101 SRA
    -- 0110 OR
    -- 0111 AND
    -- 1000 DISTINTO
    -- 1001 MENOR
    -- 1010 MAYORIGUAL
    -- 1011 MENORsinsigno
    -- 1100 MAYORIGUALsinsigno
      
----------------------------------------------------------------------------------
            
    ALUop_o <= "0000" when ((opcode_i = "0110011" and func3="000" and func7="0000000") or (opcode_i = "0110011" and func3="011" and func7="0001000") or (opcode_i = "0110011" and func3="011" and func7="0001100") or (opcode_i = "0000011" and func3="000") or (opcode_i = "0000011" and (func3="000" or func3="001" or func3="010" or func3="011" or func3="100" or func3="101" or func3="110")) or (opcode_i = "0010011" and func3="000") or (opcode_i = "1100111" and func3="000") or (opcode_i = "0100011" and (func3="000" or func3="001" or func3="010" or func3="111")) or (opcode_i = "0110111" )) else
               "0001" when ((opcode_i = "0110011" and func7="0100000" and func3="000" ) or (opcode_i="1100011" and func3="000" )) else
               "0010" when ((opcode_i = "0110011" and func3="001" and func7="0000000") or (opcode_i= "0010011" and func3= "001"))else
               "0011" when ((opcode_i = "0110011" and func3="100" and func7="0000000") or (opcode_i="0010011" and func3="100"))else
               "0100" when ((opcode_i = "0110011" and func3="101" and func7="0000000") or (opcode_i="0010011" and func3="101")) else
               "0101" when ((opcode_i = "0110011" and func3="101" and func7="0100000") or (opcode_i="0010011" and func3="101")) else
               "0110" when ((opcode_i = "0110011" and func3="110" and func7="0000000") or (opcode_i="0010011" and func3="110")) else
               "0111" when ((opcode_i = "0110011" and func3="111" and func7="0000000") or (opcode_i="0010011" and func3="111")) else  
               "1000" when (opcode_i = "1100111" and func3="001") else  
               "1001" when (opcode_i = "1100111" and func3="100") else
               "1010" when (opcode_i = "1100111" and func3="101") else
               "1011" when (opcode_i="1100111" and func3="110") else
               "1100" when (opcode_i="1100111" and func3="111");
    

end control_arch;
