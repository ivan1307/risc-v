import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure

CLK_PERIOD = 10

@cocotb.coroutine
def Reset(dut):
    dut.RST_i <=  0
    yield Timer(CLK_PERIOD * 1)
    dut.RST_i  <= 1
    yield Timer(CLK_PERIOD * 10)
    dut.RST_i  <= 0

@cocotb.test()
def test(dut):
    """
    Description:
        Test RISC-V Simple Implementation
    """
    cocotb.fork(Clock(dut.CLK_i, CLK_PERIOD).start())
    
    yield Reset(dut)

    #yield Timer(CLK_PERIOD * 64)
    
    for i in range(60):
        yield RisingEdge(dut.CLK_i)

        print("INSTRUCCIÒN",i+1)
        print("Instrucciòn =",dut.Instruction.value)
        print("PCOUT =",dut.PCOUT.value.integer)
        print("ZERO =",dut.ZERO.value)
        print("ALUsrc =",dut.ALUSrc.value)
        print("Bitlui =",dut.Bitlui.value)
        print("Branch =",dut.Branch.value)
        print("MemtoReg =",dut.MemtoReg.value)
        print("MemWrite =",dut.MemWrite.value)
        print("MemRead =",dut.MemRead.value)
        print("RegWrite =",dut.RegWrite.value)
        print("Mux_PC =",dut.Mux_PC.value)
        print("ImmGenOUT =",dut.ImmGenOUT.value)
        print("ALUOUT =",dut.ALUresult.value.integer)
        print("MUXtoPC=",dut.MUXtoPC.value)
        print("ALUOPERACIÒN =",dut.ALUOp.value)
