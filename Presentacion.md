# Trabajo Practico Nº4 "Camino de datos y control" #


##Introducción##
Durante el primer semestre del año 2019 en la materia Arquitectura de las Computadoras se aprendió sobre el funcionamiento y composición de un procesador con arquitectura Risc-V. Con el fin de aplicar los conceptos aprendidos, este práctico se basa en el diseño de un procesador Risc-V, el cual ejecute una cierta cantidad de instrucciones brindadas por la cátedra.

##Desarrollo del Proyecto##

El Trabajo Practico se basa de 3 partes las cuales son: Diseño del Data-path, tabla de instrucciones y programa con las instrucciones.

###Diseño del Datapath 
![enter image description here](https://lh3.googleusercontent.com/k8GXQO8n5JfPVAp9s5IJZIPGutJksxE_4hRwpmVUdWFLtyJ4Lj1mhKjL9P_I9Rtij-gA8r2K4t-z)

La implementación de Risc-V debe contemplar instrucciones en las cuales se pueda cargar información (traer información desde el mundo exterior hacia adentro,es decir, hacia un registro). El procesador solo puede realizar acciones sobre los registros, si tengo información afuera se tendrá que ingresar.

Las operaciones lógicas aritméticas que realiza este procesador son: suma, resta, and, or y xor con las cuales se contempla casi todas las instrucciones. También se realizan instrucciones de desplazamientos e instrucciones de saltos. 


El procesador puede sacar o puede introducir información a la memoria de datos. Otras de las funciones que realiza es sacar cuentas mediante 2 operandos cuyo origen es de un banco de registros o directamente embebido en la instrucción. El resultado de la cuenta sale directamente a través de la ALU, porque es la única parte del procesador que realiza cuentas. Hay un par de sumadores los cuales se utiliza para tareas especificas.

El primer multiplexor es el que alimenta al contador de programa. El valor de entrada es seleccionado por una señal de control directamente relacionado con la instrucción que se esta ejecutando. De esta manera el contador de programa apunta a la siguiente instrucción.

En uno de los sumadores se suma 4 debido a que tiene direccionamiento a byte, es decir, la mayoría de las instrucciones de Risc-V son de 32 bits y el ancho de la memoria es de 8bits, por lo tanto cada instrucción ocupa 4 lugares de 8bits.

La unidad de control le indica a cada parte del procesador la función que debe realizar. Para que pueda tomar decisiones se implementan estructuras condicionales. Este control lo realiza activando y desactivando señales de acuerdo a la instrucción en ejecución. 

Este procesador es monociclo, lo que significa que en 1 ciclo se ejecuta 1 instrucción. El tiempo mas largo va a ser tan largo como la instrucción más larga (Ld), debido a que usa todos los recursos del Data Path.

###Tabla de instrucciones
La siguiente tabla están las instrucciones que ejecuta el procesador. Se observa que cada operación lógica se le asigno un código de 4 bits para identificar la operación que debe realizar. Tambien se anexaron las señales de control que determinan la ejecución de cada instrucción.

![enter image description here](https://lh3.googleusercontent.com/vjQMNKz2N3twtbigjaMmKNHmkubRHFB_A7irtSBTwVpZwdVURI9ObM50VS24-p-lCzT_atWPx8L9)

###Programa
El programa que se ejecuta es el siguiente:

          addi $x1,$x0,10       	//$x1=10
          addi $x2,$x0,3 			//$x2=3
          add $x3,$x1,$x2			//$x3=13
          addi $x4,$x0,1			//$x4=1
          sub $x2,$x2,$x4			//$x2=2
    bucle1:beq $x2,x4,salto1        //si $x2=$x4, salta
          sub $x2,$x2,$x4		    //$x2=1
          jal $x6,bucle1			//$x6=PC, salta a bucle1
    salto1:slli $x1,$x1,1		    //$x1=20
          xori $x2,$x3,3			//$x2=14
          srli $x2,$x2,3		    //$x2=1
          srai $x7,$x3,2            //$x7=3
          ori $x8,$x7,4		        //$x8=7
          andi $x9,$x7,1	        //$x9=1
          jalr $x10,18($x0)  	//$x10=PC, salta a PC=72(sll)
          addi $x0,$x0,0 		    //$x0=0
          addi $x0,$x0,0 		    //$x0=0
          addi $x0,$x0,0 		    //$x0=0
          sll $x11,$x9,$x7		    //$x11=8
          xor $x12,$x11,$x7         //$x12=11
          srl $x13,$x8,$x9          //$x13=3
          sra $x13,$x13,$x9         //$x13=1
          or $x14,$x7,$x9           //$x14=3
          and $x15,$x7,$x9          //$x15=1
          sd $x14,10($x0)   	    //Memory[$x0+10]=3
          ld $x16,10($x0)      //$x16=Memory[$x0+10], $x16=3
          lui $x17,5		        //$x17=20480
    bucle2:bne $x7,$x16,salto2    //si $x7!=$x16, salta
          addi $x7,$x7,1		    //$x7=4
          jal $x18,bucle2		    //$x18=PC, salta a bucle2
    salto2:blt $x7,$x16,salto3	  //si $x7<$x16, salta
          addi $x16,$x16,4		    //$x16=7
          jal $x19,salto2	        //$x19=PC, salta a salto2
    salto3:bge $x7,$x16,etiqueta  //si $x7>=$x16, salta
          addi $x7,$x7,10	        //$x7=14
          jal $x20,salto3		  //$x20=PC, salta a salto3
    etiqueta:addi $x0,$x0,0	        //$x0=0
          addi $x21,$x0,-10		    //$x21=-10
          bltu $x21,$x7,salto4	  //si u($x21)<u($x7), salta
          addi $x0,$x0,0		  	//$x0=0
          addi $x0,$x0,0	        //$x0=0
    salto4:bgeu $x2,$x7,salto5	  //si u($x2)>=u($x7), salta 
          addi $x7,$x7,110		    //$x7=124
          jal $x22,salto4		  //$x22=PC, salta a salto4
    salto5:addi $x0,$x0,0	        //$x0=0
          
         
      
## Conclusión ##

El Trabajo que realizamos ha contribuido de manera muy importante para identificar y resaltar los puntos que hay que cubrir y considerar para llevar acabo una implementación exitosa de un procesador Risc-V. Nos deja muchas cosas importantes que aprendimos y que hemos reforzado para llevar a cabo una buena implementación. 


          



          

















  
